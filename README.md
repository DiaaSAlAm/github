A GitHub repository search app, created as an iOS recruitment task for Robusta Studio.

Features
* Getting the most popular repos at the app launch
* Repository search
* Repository Details
* Unit test

Requirements
* Xcode 12.3
* iOS 13+ 
* Swift 5+ 
 

Project Overview

Connecting to the GitHub API, the app shows a collection view of repos that match the user's query. When tapping a repo cell, the user is taken to a detail view controller, where they can find the info about the repo's:

* Storyboards/Nibs.
* Auto Layout.
* URLSession Network part. 
 
 task management tool 
* Trello (https://trello.com/b/agbzFvYx/github) 

