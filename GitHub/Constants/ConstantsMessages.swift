//
//  ConstantsMessages.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import Foundation

struct ConstantsMessages {
    static let genericErrorMessage = "Something went wrong please try again later"
    static let noRepLoaded = "No Repositories Loaded"
}
