//
//  ConstantsCellIdentifier.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import Foundation

enum ConstantsCellIdentifier: String{
    case repositoriesCell = "RepositoriesCell"
}
