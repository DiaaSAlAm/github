//
//  AppTheme.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import UIKit

/// Apply application theme and colors
class AppTheme {
    
    static func apply() {
        navigationBarStyle()
        searchBarStyle()
    }
    
    private static func navigationBarStyle() { 
        UINavigationBar.appearance().barTintColor = .white
        UINavigationBar.appearance().tintColor = .black
        UINavigationBar.appearance().titleTextAttributes = [
            NSAttributedString.Key.foregroundColor: UIColor.black
        ]
        UINavigationBar.appearance().isTranslucent = false
    }
    
    private static  func searchBarStyle() {
        UISearchBar.appearance().backgroundColor = .white
        UISearchBar.appearance().tintColor = .black
       
        
    }
}
