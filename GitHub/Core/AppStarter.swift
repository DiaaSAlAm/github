//
//  AppStarter.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import UIKit

/// AppStarter here you can handle everything before letting your app starts
final class AppStarter {
    static let shared = AppStarter()
    
    private init() {}
    
    func start(window: UIWindow?) {
        AppTheme.apply()
        setRootViewController(window: window)
    }
    
    private func setRootViewController(window: UIWindow?) {
        let rootViewController = UINavigationController(rootViewController: RepositoriesListRouter.createModule())
        window?.rootViewController = rootViewController
        window?.makeKeyAndVisible()
    }
}
