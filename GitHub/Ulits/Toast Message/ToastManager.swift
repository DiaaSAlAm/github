//
//  ToastManager.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

 
import UIKit

enum ToastMessageKind {
    case info , error, success
}

class ToastManager {
    //MARK: Properties
    static let shared = ToastManager()
    private let window = (SceneDelegate.shared?.window)
    private var message: String = ""
    private var bottomAnchor: NSLayoutConstraint!
    private var messageHeaders: [ToastView?] = []
    
    //MARK: Methods
    private init() {}
    
    
    func showMessage(messageKind: ToastMessageKind, message: String, completion: (() -> Void)? = nil) {
        let messageHeader: ToastView? = ToastView()
        messageHeaders.forEach({
            hideBanner(messageHeaders: $0)
        })
        messageHeaders.append(messageHeader)
        setupView(messageHeader: messageHeader ?? ToastView(), messageKind: messageKind)
        self.message = message
        createBannerWithInitialPosition(messageHeader: messageHeader)
        DispatchQueue.main.asyncAfter(deadline: .now() + 3) { [weak self] in
            self?.hideBanner(messageHeaders: messageHeader)
            completion?()
        }
    }
    
    private func setupView(messageHeader: ToastView, messageKind: ToastMessageKind){
        switch messageKind {
        case .success:
            messageHeader.contentView.backgroundColor = .black
            messageHeader.msgLabel.textColor = .white
        case .error:
            messageHeader.contentView.backgroundColor = .red
            messageHeader.msgLabel.textColor = .white
        case .info:
            messageHeader.contentView.backgroundColor = .yellow
            messageHeader.msgLabel.textColor = .white
        }
    }
    
    private func createBannerWithInitialPosition(messageHeader: ToastView?) {
        guard let messageHeader = messageHeader else { return }
        guard let window = window else {return}
        messageHeader.msgLabel.text = message
        messageHeader.layer.cornerRadius = 10
        messageHeader.layer.masksToBounds = true
        window.addSubview(messageHeader)
        let guide = window.safeAreaLayoutGuide
        messageHeader.translatesAutoresizingMaskIntoConstraints = false
        bottomAnchor = messageHeader.bottomAnchor.constraint(equalTo: guide.bottomAnchor, constant: 100)
        bottomAnchor.isActive = true
        messageHeader.trailingAnchor.constraint(lessThanOrEqualTo: guide.trailingAnchor, constant: -20).isActive = true
        messageHeader.leadingAnchor.constraint(greaterThanOrEqualTo: guide.leadingAnchor, constant: 20).isActive = true
        messageHeader.centerXAnchor.constraint(equalTo: window.centerXAnchor).isActive = true
        messageHeader.heightAnchor.constraint(equalToConstant: messageHeader.viewHeight).isActive = true
        window.layoutIfNeeded()
        animateBannerPresentation()
    }
    
    private func animateBannerPresentation() {
        if KeyboardStateManager.shared.isVisible {
            bottomAnchor.constant = -KeyboardStateManager.shared.keyboardOffset
        } else {
            bottomAnchor.constant = -20
        }
        UIView.animate(withDuration: 0.3, delay: 0.1, options: [], animations: { [weak self] in self?.window!.layoutIfNeeded() }, completion: nil)
    }
    
    private func hideBanner(messageHeaders: ToastView?) {
        UIView.animate(withDuration: 0.5, animations: {
            messageHeaders?.alpha = 0
        }) { _ in
            messageHeaders?.removeFromSuperview()
        }
    }
}
