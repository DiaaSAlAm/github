//
//  ExArray.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import Foundation

extension Array {
    func getElement(at index: Int) -> Element? {
        let isValidIndex = index >= 0 && index < count
        return isValidIndex ? self[index] : nil
    }
}
