//
//  ExString.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import Foundation

extension String {
    func isEmptyOrWhitespace() -> Bool { //Check if text is empty or with white Soaces "    "
        if(self.isEmpty) {
            return true
        }
        return (self.trimmingCharacters(in: NSCharacterSet.whitespaces) == "")
    }
    
    static func convertStringToDate(date: String) -> Date{
        let dateFormatter = DateFormatter()
        dateFormatter.locale = Locale(identifier: "en_US_POSIX")
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        return dateFormatter.date(from:date)!
    }
    
    static func convertDateFormater(_ date: String) -> String{
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ssZ"
        guard let date = dateFormatter.date(from: date) else { return "" }
        dateFormatter.dateFormat = "EEEE, MMM d, yyyy"
        return  dateFormatter.string(from: date)
        
    }
    
   static func getElapsedInterval(_ dateString: String) -> String {
        let date = convertStringToDate(date: dateString)
        let interval = Calendar.current.dateComponents([.year, .month, .day], from: date, to: Date())
        
        if let year = interval.year, year > 0 {
            return convertDateFormater(dateString)
        } else if let month = interval.month, month > 0, month <= 6 {
            return month == 1 ? "\(month)" + " " + "month ago" :
                "\(month)" + " " + "months ago"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "day ago" :
                "\(day)" + " " + "days ago"
        } else {
            return "a moment ago"
            
        }
    }
}
 
