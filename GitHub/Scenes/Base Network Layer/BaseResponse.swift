//
//  BaseResponse.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

class BaseResponse<T: Codable>: Codable {
    var totalCount: Int?
    var items: T?
    var incompleteResults: Bool?
    
    enum CodingKeys: String, CodingKey {
        case totalCount = "total_count"
        case items = "items"
        case incompleteResults = "incomplete_results"
    }
}
