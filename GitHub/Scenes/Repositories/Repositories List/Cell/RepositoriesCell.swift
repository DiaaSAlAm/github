//
//  RepositoriesCell.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import UIKit

class RepositoriesCell: UICollectionViewCell, RepositoriesListCellViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var fullNameLb: UILabel!
    @IBOutlet private weak var creationDateLb: UILabel!

    //MARK: - Life Cycle
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    //MARK: - Configure Cell
    func configureCell(viewModel: RepositoriesListViewModel) {
        fullNameLb.text = viewModel.fullName
        creationDateLb.text = viewModel.createdAt ?? "."
        avatar.loadImageAsync(with: viewModel.avatar ?? "")
    }

}
