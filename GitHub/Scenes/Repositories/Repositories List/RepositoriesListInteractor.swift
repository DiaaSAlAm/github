//
//  RepositoriesListInteractor.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import Foundation

class RepositoriesListInteractor: RepositoriesListInteractorInputProtocol {

    weak var presenter: RepositoriesListInteractorOutputProtocol?
    private let repositoriesNetwork: RepositoriesAPIProtocol = RepositoriesAPI()
    
    func searchRepositories(query: String,page: Int){
        repositoriesNetwork.searchRepositories(query: query, page: page) { [weak self] (result) in
          guard let self = self else {return}
          switch result {
          case .success(let response):
            guard let model = response?.items , model.count != 0 else {
                self.presenter?.failedRequest(withError:  ConstantsMessages.noRepLoaded)
                return
            }
            self.presenter?.successRequest(model: model, incompleteResults: response?.incompleteResults ?? false)
          case .failure(let error):
              let err = error.localizedDescription
              print(err)
            self.presenter?.failedRequest(withError: ConstantsMessages.genericErrorMessage)
          }
        }
    }
     
}
