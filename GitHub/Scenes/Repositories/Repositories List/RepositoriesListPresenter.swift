//
//  RepositoriesListPresenter.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import UIKit

class RepositoriesListPresenter: RepositoriesListPresenterProtocol, RepositoriesListInteractorOutputProtocol {
    
    weak var view: RepositoriesListViewProtocol?
    private let interactor: RepositoriesListInteractorInputProtocol
    private var router: RepositoriesListRouterProtocol
    var repositoriesListModel = [RepositoriesListModel]()
    private var incompleteResults: Bool?
    private var page = 1
    private var query = "stars:>100" //getting the most popular repos at the app launch.
    var numberOfRow: Int { return repositoriesListModel.count}
    
    init(view: RepositoriesListViewProtocol, interactor: RepositoriesListInteractorInputProtocol, router: RepositoriesListRouterProtocol) {
        self.view = view
        self.interactor = interactor
        self.router = router
    }
     
    func searchRepositories(query: String){
        repositoriesListModel.removeAll()
        getRepositories(query: query)
     }
    
    func getRepositories(query: String){
        view?.showLoadingIndicator()
        interactor.searchRepositories(query: query, page: page)
    }
    
    func viewDidLoad(){
        getRepositories(query: query)
    }
    
    func fetchNextPage(){
        guard incompleteResults == false else {return}
        page = page + 1
        getRepositories(query: query)
    }
     
    
    func configureCell(cell: RepositoriesListCellViewProtocol, indexPath: IndexPath) {
        guard let data = repositoriesListModel.getElement(at: indexPath.row) else {return}
        let viewModel = RepositoriesListViewModel(data: data)
        cell.configureCell(viewModel: viewModel)
    }
    
    func successRequest(model: [RepositoriesListModel],incompleteResults: Bool) {
        self.incompleteResults = incompleteResults
        repositoriesListModel.append(contentsOf: model)
        view?.hideLoadingIndicator()
        view?.successRequest()
    }
    
    func failedRequest(withError error: String) {
         view?.hideLoadingIndicator()
        router.showMessage(message: error, messageKind: .error)
    }
    
    func showMessage(message: String) {
        router.showMessage(message: message, messageKind: .success)
    }
    
    func navigateRepositoryDetails(_ row: Int){
        guard let model = repositoriesListModel.getElement(at: row) else {return}
        router.openRepositoryDetails(repositoriesListModel: model)
    }
     
}
