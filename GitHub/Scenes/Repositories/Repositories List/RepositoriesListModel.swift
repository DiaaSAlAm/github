//
//  RepositoriesListModel.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//
 
 
import Foundation

struct RepositoriesListModel: Codable {
    var fullName: String?
    var repoName: String?
    var avatar: String?
    var createdAt: String?
    var owner: Owner?
    var language: String?
    var forks: Int?
    var openIssues: Int?
    var htmlUrl: String?
    
    enum CodingKeys: String, CodingKey {

        case fullName = "full_name"
        case repoName = "name"
        case avatar = "avatar_url"
        case createdAt = "created_at"
        case owner = "owner"
        case language = "language"
        case forks = "forks"
        case openIssues = "open_issues"
        case htmlUrl = "html_url"
    }
}

struct Owner : Codable {
    let avatarUrl : String?

    enum CodingKeys: String, CodingKey {
 
        case avatarUrl = "avatar_url"
    }
}

struct RepositoriesListViewModel {
    var fullName: String?
    var repoName : String?
    var avatar: String?
    var createdAt: String?
    
    init(data: RepositoriesListModel) {
        self.fullName = data.fullName
        self.repoName = data.repoName
        self.avatar = data.owner?.avatarUrl ?? ""
        self.createdAt =  String.getElapsedInterval(data.createdAt ?? "")
    }
}
 
