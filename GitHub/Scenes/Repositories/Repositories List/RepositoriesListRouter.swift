//
//  RepositoriesListRouter.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import UIKit
 
class RepositoriesListRouter: RepositoriesListRouterProtocol { //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    
    static func createModule() -> UIViewController {
        let view = UIStoryboard(name: ConstantsStoryboardName.repositories.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(RepositoriesListVC.self)") as! RepositoriesListVC
        let interactor = RepositoriesListInteractor()
        let router = RepositoriesListRouter()
        let presenter = RepositoriesListPresenter(view: view, interactor: interactor, router: router)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func showMessage(message: String, messageKind: ToastMessageKind) {
        DispatchQueue.main.async {
            ToastManager.shared.showMessage(messageKind: messageKind, message: message)
        }
    }

    func openRepositoryDetails(repositoriesListModel: RepositoriesListModel){
        let view = RepositoryDetailsRouter.createModule(repositoriesListModel: repositoriesListModel)
        viewController?.navigationController?.pushViewController(view, animated: true)
    }
}
