//
//  RepositoriesListVC.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import UIKit
 
class RepositoriesListVC: UIViewController, RepositoriesListViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var searchBar: UISearchBar!
    @IBOutlet private weak var collectionView: UICollectionView!
    
    //MARK: - Properties
    var presenter: RepositoriesListPresenterProtocol!
    private let cellIdentifier = ConstantsCellIdentifier.repositoriesCell.rawValue
    private let activityView = UIActivityIndicatorView(style: .large)
    private let fadeView: UIView = UIView()
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    func showLoadingIndicator() {
        DispatchQueue.main.async {
            self.startAnimatingActivityView()
        }
    }
    
    func hideLoadingIndicator() {
        DispatchQueue.main.async {
            self.stopAnimatingActivityView()
        }
    }
    
    func successRequest() {
        DispatchQueue.main.async {
            self.collectionView.reloadData()
        }
    }
}

//MARK: Setup View
extension RepositoriesListVC {
    
     fileprivate func setupUI() {
        title = "Repositories"
        registerCollectionView()
       addUITapGestureRecognizer()
        searchBar.delegate = self
        presenter.viewDidLoad()
     }
    
    //MARK: - Start Animating Activity
    fileprivate func startAnimatingActivityView() {
        fadeView.frame = self.collectionView.frame
        fadeView.backgroundColor = .white
        fadeView.alpha = 0.6
        self.view.addSubview(fadeView)
        
        activityView.hidesWhenStopped = true
        activityView.center = self.view.center
        activityView.startAnimating()
        self.view.addSubview(activityView)
    }
    
    //MARK: - Stop Animating Activity
    fileprivate func stopAnimatingActivityView() {
        self.fadeView.removeFromSuperview()
        self.activityView.stopAnimating()
    }
    
    //MARK: - Register  CollectionView Cell
    fileprivate func registerCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(UINib(nibName: cellIdentifier, bundle: nil), forCellWithReuseIdentifier: cellIdentifier)
    }
    
    //MARK: - add Tap Gesture handle tapping empty space (not a cell)
    func addUITapGestureRecognizer(){
        //for single or multiple taps.
        let tap = UITapGestureRecognizer(target: self, action: #selector(UIInputViewController.dismissKeyboard))
        tap.delegate = self
        self.collectionView.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

 
//MARK: - UICollectionView Delegate
extension RepositoriesListVC: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        defer { collectionView.deselectItem(at: indexPath, animated: true) }
        dismissKeyboard()
        presenter.navigateRepositoryDetails(indexPath.row)
    }
}

//MARK: - UICollectionView Data Source
extension RepositoriesListVC: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return presenter.numberOfRow
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellIdentifier,for: indexPath) as! RepositoriesCell
       presenter.configureCell(cell: cell, indexPath: indexPath)
        return cell
    }
}

//MARK: - UICollectionView Delegate Flow Layout
extension RepositoriesListVC: UICollectionViewDelegateFlowLayout {
    
    //MARK: - Load new item if Available
    func collectionView(_ collectionView: UICollectionView,
                        willDisplay cell: UICollectionViewCell,
                        forItemAt indexPath: IndexPath) {
        if  indexPath.item == presenter.numberOfRow - 3 {
            presenter.fetchNextPage()
        }
    }
    
    func collectionView(
        _ collectionView: UICollectionView,
        layout collectionViewLayout: UICollectionViewLayout,
        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width = collectionView.frame.width
        return CGSize(width: width , height: 100)
    }
}

//MARK: Search Bar Delegate
extension RepositoriesListVC : UISearchBarDelegate {
    
    //MARK: - Search Button Clicked
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        guard let searchBarText = searchBar.text, !searchBar.text!.isEmptyOrWhitespace() else {return}
        dismissKeyboard()
        presenter.searchRepositories(query: searchBarText)
    }
    
    //MARK: - Cancel Button Clicked -> Reload most popular repos
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        dismissKeyboard()
        searchBar.text = ""
        presenter.searchRepositories(query: "stars:>100")
    } 
}

extension RepositoriesListVC: UIGestureRecognizerDelegate {
    
    func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        // only handle tapping empty space (not a cell)
        let point = gestureRecognizer.location(in: collectionView)
        let indexPath = collectionView.indexPathForItem(at: point)
        return indexPath == nil
    }
}

 

