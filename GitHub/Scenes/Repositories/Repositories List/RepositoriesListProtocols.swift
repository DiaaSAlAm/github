//
//  RepositoriesListProtocols.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import UIKit

protocol RepositoriesListViewProtocol: class { //View Conteroller
    var presenter: RepositoriesListPresenterProtocol! { get set }
    func showLoadingIndicator()
    func hideLoadingIndicator()
    func successRequest()
}

protocol RepositoriesListPresenterProtocol: class { // Logic
    var view: RepositoriesListViewProtocol? { get set }
    func configureCell(cell: RepositoriesListCellViewProtocol, indexPath: IndexPath)
    var numberOfRow: Int {get} 
    func searchRepositories(query: String)
    func fetchNextPage()
    func viewDidLoad()
    func navigateRepositoryDetails(_ row: Int)
}

protocol RepositoriesListInteractorInputProtocol: class { // func do it from presenter
     var presenter: RepositoriesListInteractorOutputProtocol? { get set }
    func searchRepositories(query: String,page: Int)
}

protocol RepositoriesListInteractorOutputProtocol: class { // it's will call when interactor finished
    func successRequest(model: [RepositoriesListModel],incompleteResults: Bool)
    func failedRequest(withError error: String)
    func showMessage(message: String)
}

protocol RepositoriesListRouterProtocol { // Navigate
    func showMessage(message: String, messageKind: ToastMessageKind)
    func openRepositoryDetails(repositoriesListModel: RepositoriesListModel)
}

extension RepositoriesListRouterProtocol { // Make some protocol optional
    func showMessage(message: String, messageKind: ToastMessageKind) {
    }
}
 
protocol RepositoriesListCellViewProtocol { // it's will call when register cell
    func configureCell(viewModel: RepositoriesListViewModel)
}
