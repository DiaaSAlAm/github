//
//  RepositoryDetailsVC.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/21/21.
//

import UIKit 

class RepositoryDetailsVC: UIViewController, RepositoryDetailsViewProtocol {
    
    //MARK: - IBOutlets
    @IBOutlet private weak var avatar: UIImageView!
    @IBOutlet private weak var fullNameLb: UILabel!
    @IBOutlet private weak var creationDateLb: UILabel!
    @IBOutlet private weak var languageLb: UILabel!
    @IBOutlet private weak var forksLb: UILabel!
    @IBOutlet private weak var openIssuesLb: UILabel!
    
    
    //MARK: - Properties
    var presenter: RepositoryDetailsPresenterProtocol!
    
    //MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setData()
    } 
    
    //MARK: - IBOutlets
    @IBAction
    func didTappedViewMore(_ sender: UIButton) {
        presenter.openSafariViewController()
    }
}

//MARK: Setup View
extension RepositoryDetailsVC {
     
    fileprivate func setData(){
        title = "Details"
        avatar.loadImageAsync(with: presenter.avatar)
        fullNameLb.text = presenter.fullName
        creationDateLb.text = presenter.createdAt
        languageLb.text = presenter.language
        forksLb.text = presenter.forks
        openIssuesLb.text = presenter.openIssues
    }
    
    
}

 
