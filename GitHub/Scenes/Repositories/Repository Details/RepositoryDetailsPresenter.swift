//
//  RepositoryDetailsPresenter.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/21/21.
//

import UIKit

class RepositoryDetailsPresenter: RepositoryDetailsPresenterProtocol, RepositoryDetailsInteractorOutputProtocol {
    
    weak var view: RepositoryDetailsViewProtocol?
    private let interactor: RepositoryDetailsInteractorInputProtocol
    private var router: RepositoryDetailsRouterProtocol
    private var repositoriesListModel: RepositoriesListModel?
    var fullName: String { return repositoriesListModel?.fullName ?? "" }
    var avatar: String { return repositoriesListModel?.owner?.avatarUrl ?? "" }
    var date: String {return repositoriesListModel?.createdAt ?? "" }
    var createdAt: String { return String.getElapsedInterval(repositoriesListModel?.createdAt ?? "" ) }
    var language: String {return "Language:  \(repositoriesListModel?.language ?? "")" }
    var forks: String {return "Forks: \(repositoriesListModel?.forks ?? 0)" }
    var openIssues: String {return "Open issues: \(repositoriesListModel?.openIssues ?? 0)" }
    private var htmlUrl: String { return repositoriesListModel?.htmlUrl ?? ""}
    
    init(view: RepositoryDetailsViewProtocol, interactor: RepositoryDetailsInteractorInputProtocol, router: RepositoryDetailsRouterProtocol, repositoriesListModel: RepositoriesListModel) {
        self.view = view
        self.interactor = interactor
        self.router = router
        self.repositoriesListModel = repositoriesListModel
    }
    
    func openSafariViewController(){
        router.openSafariViewController(htmlUrl: htmlUrl)
    }
    
     
}
