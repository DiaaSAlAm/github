//
//  RepositoryDetailsRouter.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/21/21.
//

import UIKit
import SafariServices
 
class RepositoryDetailsRouter: RepositoryDetailsRouterProtocol { //Router is only one take dicret instance
    

    weak var viewController: UIViewController?
    
    static func createModule(repositoriesListModel: RepositoriesListModel) -> UIViewController {
        let view = UIStoryboard(name: ConstantsStoryboardName.repositories.rawValue, bundle: nil).instantiateViewController(withIdentifier: "\(RepositoryDetailsVC.self)") as! RepositoryDetailsVC
        let interactor = RepositoryDetailsInteractor()
        let router = RepositoryDetailsRouter()
        let presenter = RepositoryDetailsPresenter(view: view, interactor: interactor, router: router, repositoriesListModel: repositoriesListModel)
        view.presenter = presenter
        interactor.presenter = presenter
        router.viewController = view
        return view
    }
    
    func openSafariViewController(htmlUrl: String){
        guard let url = URL(string: htmlUrl) else { return }
        let svc = SFSafariViewController(url: url)
        viewController?.present(svc, animated: true, completion: nil)
    }

}
