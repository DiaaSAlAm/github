//
//  RepositoryDetails.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/21/21.
//

import UIKit

protocol RepositoryDetailsViewProtocol: class { //View Conteroller
    var presenter: RepositoryDetailsPresenterProtocol! { get set }
}

protocol RepositoryDetailsPresenterProtocol: class { // Logic
    var view: RepositoryDetailsViewProtocol? { get set }
    var fullName: String { get }
    var avatar: String { get }
    var date: String { get }
    var createdAt: String { get }
    var language: String { get }
    var forks: String { get }
    var openIssues: String { get }
    func openSafariViewController()
}

protocol RepositoryDetailsInteractorInputProtocol: class { // func do it from presenter
     var presenter: RepositoryDetailsInteractorOutputProtocol? { get set }
    
}

protocol RepositoryDetailsInteractorOutputProtocol: class { // it's will call when interactor finished
}

protocol RepositoryDetailsRouterProtocol { // For Segue
    func openSafariViewController(htmlUrl: String)
}
