//
//  RepositoriesAPI.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import Foundation

protocol RepositoriesAPIProtocol {
    
    func searchRepositories(query: String,page: Int,completion: @escaping (Result<BaseResponse<[RepositoriesListModel]>?, NSError>) -> Void)
    
    func getRepositories(completion: @escaping (Result<[RepositoriesListModel]?, NSError>) -> Void)
}


class RepositoriesAPI: BaseAPI<RepositoriesNetwork>, RepositoriesAPIProtocol {
    func searchRepositories(query: String, page: Int, completion: @escaping (Result<BaseResponse<[RepositoriesListModel]>?, NSError>) -> Void) {
        self.fetchData(target: .searchRepositories(query: query, page: page), responseClass: BaseResponse<[RepositoriesListModel]>.self, completion: completion)
    }
    
    //MARK:- Requests
    func getRepositories(completion: @escaping (Result<[RepositoriesListModel]?, NSError>) -> Void) {
        self.fetchData(target: .getRepositories, responseClass: [RepositoriesListModel].self, completion: completion)
    } 
}
