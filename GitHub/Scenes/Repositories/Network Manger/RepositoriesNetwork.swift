//
//  RepositoriesNetwork.swift
//  GitHub
//
//  Created by Diaa SAlAm on 4/20/21.
//

import Foundation

enum RepositoriesNetwork {
    case getRepositories //I void this Endpoint not found the repositories creation date in response
    case searchRepositories(query: String,page: Int)
}

extension RepositoriesNetwork: TargetType {
    
    var baseURL: String {
        return Environment.rootURL.absoluteString
    }
    
    var contentType: String {
        return "application/json"
    }
    
    
    var path: String {
        switch self {
        case .getRepositories:
            return "repositories"
        case .searchRepositories:
            return "search/repositories"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        default:
            return .get
        }
    }
    
    var headers: [String : String]? {
        switch self {
        default:
            return ["Content-Type": contentType]
        }
    }
    
    var task: Task {
        switch self {
        case .searchRepositories(let query,let page):
            return .requestParametersURLEncoding(parameters: ["q" : query, "per_page": 10, "page": page])
        default:
            return .requestPlain
        }
    }
}
