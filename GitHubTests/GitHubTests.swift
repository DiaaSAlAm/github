//
//  GitHubTests.swift
//  GitHubTests
//
//  Created by Diaa SAlAm on 4/20/21.
//

import XCTest
@testable import GitHub

class GitHubTests: XCTestCase {
    
    class MockRouter: RepositoriesListRouterProtocol {
        var repositoriesListModel: RepositoriesListModel? 
        
        func openRepositoryDetails(repositoriesListModel: RepositoriesListModel){
            self.repositoriesListModel = repositoriesListModel
        }
    }
    
    class MockInterface: RepositoriesListViewProtocol {
        var presenter: RepositoriesListPresenterProtocol!
        var shouldLoadRepositoriesList = false
        var shouldShowLoadingIndicator = false
        var shouldHideLoadingIndicator = false
        
        func showLoadingIndicator() {
            shouldShowLoadingIndicator = true
        }
        
        func hideLoadingIndicator() {
            shouldHideLoadingIndicator = true
        }
        
        func successRequest() {
            shouldLoadRepositoriesList = true
        }
    }
    
    var presenter: RepositoriesListPresenter?
    let mockInteractor = RepositoriesListInteractor()
    let mockRouter = MockRouter()
    let fakeRepositories = [RepositoriesListModel(fullName: "we", repoName: "we", avatar: "avatar", createdAt: "2015-02-15T20:30:24Z", owner: nil, language: "Swift", forks: 12, openIssues: 2, htmlUrl: "https://github.com/Tibolte/ElasticDownload")]
    var mockInterface = MockInterface()
     
    override func setUp() {
        presenter = RepositoriesListPresenter(view: mockInterface, interactor: mockInteractor, router: mockRouter)
        mockInterface.presenter = presenter
        mockInteractor.presenter = presenter
    }
    
    func testRepositoriesListCountIs1() {
        presenter?.successRequest(model: fakeRepositories, incompleteResults: false)
        XCTAssertEqual(presenter?.numberOfRow, 1)
    }
    
    func testRepositoryAtIndexIsInjectedRepository() {
        presenter?.successRequest(model: fakeRepositories, incompleteResults: false)
        let movie = presenter?.repositoriesListModel.getElement(at: 0)
        XCTAssertEqual(movie?.fullName, fakeRepositories[0].fullName)
        XCTAssertEqual(movie?.forks, fakeRepositories[0].forks)
    }
    
    func testRepositoryListEmptyShouldShowError() {
        presenter?.failedRequest(withError: "")
        XCTAssertEqual(mockInterface.shouldHideLoadingIndicator,
                       true)
        XCTAssertEqual(mockInterface.shouldLoadRepositoriesList,
                       false)
    }
    
    func testRepositoryListEmptyShouldShowMovie() {
        presenter?.successRequest(model: fakeRepositories, incompleteResults: false)
        XCTAssertEqual(mockInterface.shouldHideLoadingIndicator,
                       true)
        XCTAssertEqual(mockInterface.shouldLoadRepositoriesList,
                       true)
    }
    
    func testSelectedRepository() {
        presenter?.successRequest(model: fakeRepositories, incompleteResults: false)
        presenter?.navigateRepositoryDetails(0)
        XCTAssertEqual(mockRouter.repositoriesListModel?.fullName, fakeRepositories[0].fullName)
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
